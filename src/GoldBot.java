

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Random;

/** A bot for mining gold in https://cponline.pw/play/
 * 
 * @author Reuben Diaz
 *
 */
public class GoldBot {

	// How long to wait for a click to resolve.
	public static final int SLEEP_TIME = 1000;
	
	// The shade of brown used for the mine floor.
	public static final Color MINE_FLOOR_COLOR = new Color(-11256261);
	
	// The boundaries of the mine. These will change depending
	// on the monitor size and the position of the game window.
	public static final int MINE_BOTTOM = 700;
	public static final int MINE_TOP = 900;
	public static final int MINE_LEFT_BOUND = 650;
	public static final int MINE_RIGHT_BOUND = 1000;
	
	// The coordinates of the action menu.
	public static final int ACTION_MENU_X = 580;
	public static final int ACTION_MENU_Y = 1025;
	
	// The coordinates of the "dance" button.
	public static final int DANCE_BUTTON_X = 580;
	public static final int DANCE_BUTTON_Y = 660;

	// For controlling the mouse.
	private int x = 0, y = 0;
	private final Robot robot;
	private final Random random;
	
	// A constructor.
	public GoldBot() throws AWTException {
		this.robot = new Robot();
		this.random = new Random();
	}
	
	// Mines gold forever.
	public void mainLoop() throws InterruptedException {
		while(true) {
			moveToRandomCoordinates();
			dance();
			clickSelf();
			Thread.sleep(10000);
		}
	}

	// Moves the penguin to random coordinates in the mine.
	public void moveToRandomCoordinates() throws InterruptedException {
		// Pick a random spot in the mine.
		x = random.nextInt(MINE_TOP - MINE_BOTTOM) + MINE_BOTTOM;
		y = random.nextInt(MINE_RIGHT_BOUND - MINE_LEFT_BOUND) + MINE_LEFT_BOUND;
		
		// Get the color of the pixel we might move to.
		Color color = robot.getPixelColor(x, y);

		// If we're hovering over the mine floor, click to move.
		if (color.getRGB() == MINE_FLOOR_COLOR.getRGB()) {
			System.out.println("Moving to coordinates (" + x + ", " + y + ")");

			robot.mouseMove(x, y);
			Thread.sleep(SLEEP_TIME);

			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
			Thread.sleep(SLEEP_TIME);
		}

		// Otherwise, we're hovering over a penguin, so try picking again.
		else {
			System.out.println("Wrong color! Expected (" + MINE_FLOOR_COLOR.getRGB() + "), but got (" + color.getRGB() + ").");
			moveToRandomCoordinates();
		}
	}
	
	// Clicks on my penguin, so we can see the HUD.
	public void clickSelf() throws InterruptedException {
		System.out.println("Clicking on self to display HUD.");

		y -= 15;
		robot.mouseMove(x, y);
		Thread.sleep(SLEEP_TIME);
		
		robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		Thread.sleep(SLEEP_TIME);
	}

	// Makes the penguin dance.
	public void dance() throws InterruptedException {
		System.out.println("Dancing...");

		// Press "D" to dance.
		robot.keyPress(KeyEvent.VK_D);
		robot.keyRelease(KeyEvent.VK_D);
		
		Thread.sleep(SLEEP_TIME);
	}
	
	// Runs a gold mining bot forever.
	public static void main(String[] args) {
		try {
			GoldBot g = new GoldBot();
			g.mainLoop();
		} catch (Exception e) {
			System.err.println("Something funny happened.");
			return;
		}
	}
}
